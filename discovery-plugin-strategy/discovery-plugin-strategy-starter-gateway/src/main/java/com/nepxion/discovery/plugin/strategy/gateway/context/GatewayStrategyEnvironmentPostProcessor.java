package com.nepxion.discovery.plugin.strategy.gateway.context;

/**
 * EnvironmentPostProcessor 是环境处理类，它的功能是在 Spring Boot 在启动容器前就会进行环境做准备。
 * 而这个类的功能也特别简单，就是在系统变量里面添加当前系统(spring.application.type) 为 gateway。
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.StandardEnvironment;

import com.nepxion.discovery.common.constant.DiscoveryConstant;
import com.nepxion.discovery.common.entity.GatewayType;
import com.nepxion.discovery.common.entity.ServiceType;

public class GatewayStrategyEnvironmentPostProcessor implements EnvironmentPostProcessor, Ordered {
    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        if (StringUtils.equals(environment.getClass().getName(), StandardEnvironment.class.getName())) {
            System.setProperty(DiscoveryConstant.SPRING_APPLICATION_TYPE, ServiceType.GATEWAY.toString());
            System.setProperty(DiscoveryConstant.SPRING_APPLICATION_GATEWAY_TYPE, GatewayType.SPRING_CLOUD_GATEWAY.toString());
        }
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}