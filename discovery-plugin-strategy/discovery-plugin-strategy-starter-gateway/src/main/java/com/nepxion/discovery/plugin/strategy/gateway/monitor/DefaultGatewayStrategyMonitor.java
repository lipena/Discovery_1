package com.nepxion.discovery.plugin.strategy.gateway.monitor;

/**
 * Nepxion Discovery 对 Spring Cloud Gateway 监控的支持
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.springframework.web.server.ServerWebExchange;

import com.nepxion.discovery.plugin.strategy.monitor.StrategyMonitor;

public class DefaultGatewayStrategyMonitor extends StrategyMonitor implements GatewayStrategyMonitor {
    @Override
    public void monitor(ServerWebExchange exchange) {
        spanBuild();//这里是创建全链路跟踪的span对象
        //日志的输出，输出到MDC里面，用户可以在日志文件内获取到
        //使用StrategyLogger组件输出
        //输出内容包含:spanId和traceId，以及用户可以自定义的组件StrategyTracerAdapter内的map信息，以及当前服务自身的n-d-service信息
        //使用spring.application.strategy.logger.enabled开关控制来输出，默认关闭不输出
        loggerOutput();
        //同loggerOutput类似，也是StrategyLogger组件输出
        //但是这里只是用System.out.print的形式打印
        //打印内容包含:spanId和traceId，以及用户可以自定义的组件StrategyTracerAdapter内的map信息，以及当前服务自身的n-d-service信息，以及n-d-开头的蓝绿灰度控制信息
        //使用spring.application.strategy.logger.debug.enabled开关控制
        loggerDebug();
        //使用strategyAlarm组件来进行告警，会发布StrategyAlarmEvent事件，由用户自行订阅消费
        //告警的内容：spanId和traceId，以及用户可以自定义的组件StrategyTracerAdapter内的map信息，以及当前服务自身的n-d-service信息，以及n-d-开头的蓝绿灰度控制信息
        alarm();
        //使用strategyTracer组件
        //使用spring.application.strategy.tracer.enabled开关控制是否执行
        //使用spring.application.strategy.tracer.separate.span.enabled开关控制来决定是否走原生的span对象，开关打开从StrategyTracerContext.getCurrentContext().getSpan()内获取，开关关闭链路追踪工具原生的span对象
        //会将spanId和traceId，以及用户可以自定义的组件StrategyTracerAdapter内的map信息，以及当前服务自身的n-d-service信息，以及n-d-开头的蓝绿灰度控制信息，输入到全链路追踪工具的span对象里面
        spanOutput(null);
    }

    @Override
    public void release(ServerWebExchange exchange) {
        loggerClear();

        spanFinish();
    }
}