package com.nepxion.discovery.plugin.strategy.gateway.wrapper;

/**
 * 在进行远程 RPC 调用的时候，为了服务的扩展性会使用 Hystrix。使用线程隔离模式时，无法获取ThreadLocal中信息，Nepxion Discovery通过实现自定义并发策略StrategyCallableWrapper解决。
 *
 * 这个也是用装饰者模式
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @author Hao Huang
 * @version 1.0
 */

import java.util.concurrent.Callable;

import org.springframework.web.server.ServerWebExchange;

import com.nepxion.discovery.plugin.strategy.gateway.context.GatewayStrategyContext;
import com.nepxion.discovery.plugin.strategy.monitor.StrategyTracerContext;

public class DefaultGatewayStrategyCallableWrapper implements GatewayStrategyCallableWrapper {
    @Override
    public <T> Callable<T> wrapCallable(Callable<T> callable) {
        ServerWebExchange exchange = GatewayStrategyContext.getCurrentContext().getExchange();

        Object span = StrategyTracerContext.getCurrentContext().getSpan();

        return new Callable<T>() {
            @Override
            public T call() throws Exception {
                try {
                    GatewayStrategyContext.getCurrentContext().setExchange(exchange);

                    StrategyTracerContext.getCurrentContext().setSpan(span);

                    return callable.call();
                } finally {
                    GatewayStrategyContext.clearCurrentContext();

                    StrategyTracerContext.clearCurrentContext();
                }
            }
        };
    }
}