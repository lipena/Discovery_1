package com.nepxion.discovery.plugin.strategy.gateway.context;

/**
 * https://github.com/Nepxion/DiscoveryAgent/issues/5
 * todo 可以研究一下这个issues，研究一下字节码增强和GatewayStrategyContext.getCurrentContext()的关系
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class GatewayStrategyContextListener implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(GatewayStrategyContextListener.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        // 异步调用下，第一次启动在某些情况下可能存在丢失上下文的问题
        LOG.info("Initialize Gateway Strategy Context after Application started...");
        //这里在应用启动的时候(启动完成之前的动作)，通过调用getCurrentContext()方法，然后执行GatewayStrategyContext的构造函数，
        // 来提早让该GatewayStrategyContextHook完成字节码增强
        GatewayStrategyContext.getCurrentContext();
    }
}