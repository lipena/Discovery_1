package com.nepxion.discovery.plugin.strategy.gateway.configuration;

/**
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @author Ning Zhang
 * @version 1.0
 */

import org.apache.skywalking.apm.agent.core.context.TracingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

import com.nepxion.discovery.common.apollo.proccessor.ApolloProcessor;
import com.nepxion.discovery.common.consul.proccessor.ConsulProcessor;
import com.nepxion.discovery.common.etcd.proccessor.EtcdProcessor;
import com.nepxion.discovery.common.nacos.proccessor.NacosProcessor;
import com.nepxion.discovery.common.redis.proccessor.RedisProcessor;
import com.nepxion.discovery.common.zookeeper.proccessor.ZookeeperProcessor;
import com.nepxion.discovery.plugin.strategy.constant.StrategyConstant;
import com.nepxion.discovery.plugin.strategy.gateway.constant.GatewayStrategyConstant;
import com.nepxion.discovery.plugin.strategy.gateway.context.GatewayStrategyContextListener;
import com.nepxion.discovery.plugin.strategy.gateway.filter.DefaultGatewayStrategyClearFilter;
import com.nepxion.discovery.plugin.strategy.gateway.filter.DefaultGatewayStrategyRouteFilter;
import com.nepxion.discovery.plugin.strategy.gateway.filter.GatewayStrategyClearFilter;
import com.nepxion.discovery.plugin.strategy.gateway.filter.GatewayStrategyRouteFilter;
import com.nepxion.discovery.plugin.strategy.gateway.filter.SkyWalkingGatewayStrategyFilter;
import com.nepxion.discovery.plugin.strategy.gateway.monitor.DefaultGatewayStrategyMonitor;
import com.nepxion.discovery.plugin.strategy.gateway.monitor.GatewayStrategyMonitor;
import com.nepxion.discovery.plugin.strategy.gateway.processor.GatewayStrategyRouteApolloProcessor;
import com.nepxion.discovery.plugin.strategy.gateway.processor.GatewayStrategyRouteConsulProcessor;
import com.nepxion.discovery.plugin.strategy.gateway.processor.GatewayStrategyRouteEtcdProcessor;
import com.nepxion.discovery.plugin.strategy.gateway.processor.GatewayStrategyRouteNacosProcessor;
import com.nepxion.discovery.plugin.strategy.gateway.processor.GatewayStrategyRouteRedisProcessor;
import com.nepxion.discovery.plugin.strategy.gateway.processor.GatewayStrategyRouteZookeeperProcessor;
import com.nepxion.discovery.plugin.strategy.gateway.route.DefaultGatewayStrategyRoute;
import com.nepxion.discovery.plugin.strategy.gateway.route.GatewayStrategyRoute;
import com.nepxion.discovery.plugin.strategy.gateway.wrapper.DefaultGatewayStrategyCallableWrapper;
import com.nepxion.discovery.plugin.strategy.gateway.wrapper.GatewayStrategyCallableWrapper;

@Configuration
@AutoConfigureBefore(RibbonClientConfiguration.class)
public class GatewayStrategyAutoConfiguration {
    //Nepxion Discovery 实现 GlobalFilter 对 Spring Cloud Gateway 的进行扩展，同时实现自定义接口 StrategyRouteFilter 获取请求对象或者配置中心配置的灰度参数。
    @Bean
    @ConditionalOnMissingBean
    public GatewayStrategyRouteFilter gatewayStrategyRouteFilter() {
        return new DefaultGatewayStrategyRouteFilter();
    }

    //Nepxion Discovery 实现 GlobalFilter 对 Spring Cloud Gateway 的进行扩展，主要是请求调用完成之后对一些资源的释放
    @Bean
    @ConditionalOnMissingBean
    public GatewayStrategyClearFilter gatewayStrategyClearFilter() {
        return new DefaultGatewayStrategyClearFilter();
    }

    //Nepxion Discovery 对 Spring Cloud Gateway 监控的支持，和请求完成后全链路追踪工具释放资源
    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(value = StrategyConstant.SPRING_APPLICATION_STRATEGY_MONITOR_ENABLED, matchIfMissing = false)
    public GatewayStrategyMonitor gatewayStrategyMonitor() {
        return new DefaultGatewayStrategyMonitor();
    }

    //封装操作gateway网关资源(路由、断言、过滤器)的crud方法类
    @Bean
    @ConditionalOnMissingBean
    public GatewayStrategyRoute gatewayStrategyRoute() {
        return new DefaultGatewayStrategyRoute();
    }

    //在进行远程 RPC 调用的时候，为了服务的扩展性会使用 Hystrix。使用线程隔离模式时，无法获取ThreadLocal中信息，Nepxion Discovery通过实现自定义并发策略StrategyCallableWrapper解决。
    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(value = StrategyConstant.SPRING_APPLICATION_STRATEGY_HYSTRIX_THREADLOCAL_SUPPORTED, matchIfMissing = false)
    public GatewayStrategyCallableWrapper gatewayStrategyCallableWrapper() {
        return new DefaultGatewayStrategyCallableWrapper();
    }

    //解决应用刚启动完成，第一个请求就是异步调用，此时初始化的discover-agent相关类和业务调用基本同时开始。当业务调用先于Agent初始化完成，那么灰度不会生效。这种情况只会发生在第一次。
    @Bean
    public GatewayStrategyContextListener gatewayStrategyContextListener() {
        return new GatewayStrategyContextListener();
    }

    //总结，若未配置havingValue的值，matchIfMissing为true则无论是否有配置都会加载配置类，matchIfMissing为false，有配置加载类，无配置不加载类。
    //总结，只要配置了正确的havingValue值，无论matchIfMissing怎么设置，都会加载，只要配置的havingValue值不正确，无论matchIfMissing怎么设置，都不会加载。
    //开启和关闭网关订阅配置中心的动态路由策略。缺失则默认为false
    @ConditionalOnClass(NacosProcessor.class)
    @ConditionalOnProperty(value = GatewayStrategyConstant.SPRING_APPLICATION_STRATEGY_GATEWAY_DYNAMIC_ROUTE_ENABLED, matchIfMissing = false)
    protected static class GatewayRouteNacosConfiguration {
        //这个只有在spring.cloud.gateway.discovery.locator.enabled为false的时候才加载，起到的作用跟com.nepxion.discovery.plugin.configcenter.initializer.ConfigInitializer里面的initialize()作用类似
        //区别在于这里不会更改缓存的规则，只有一个监听器，用来监听nacos的规则变更，然后动态修改gateway里面的路由信息，也就是个类似的动态路由装置
        @Bean
        @ConditionalOnProperty(value = "spring.cloud.gateway.discovery.locator.enabled", havingValue = "false", matchIfMissing = true)
        public NacosProcessor gatewayStrategyRouteNacosProcessor() {
            return new GatewayStrategyRouteNacosProcessor();
        }
    }

    @ConditionalOnClass(ApolloProcessor.class)
    @ConditionalOnProperty(value = GatewayStrategyConstant.SPRING_APPLICATION_STRATEGY_GATEWAY_DYNAMIC_ROUTE_ENABLED, matchIfMissing = false)
    protected static class GatewayRouteApolloConfiguration {
        @Bean
        @ConditionalOnProperty(value = "spring.cloud.gateway.discovery.locator.enabled", havingValue = "false", matchIfMissing = true)
        public ApolloProcessor gatewayStrategyRouteApolloProcessor() {
            return new GatewayStrategyRouteApolloProcessor();
        }
    }

    @ConditionalOnClass(RedisProcessor.class)
    @ConditionalOnProperty(value = GatewayStrategyConstant.SPRING_APPLICATION_STRATEGY_GATEWAY_DYNAMIC_ROUTE_ENABLED, matchIfMissing = false)
    protected static class GatewayRouteRedisConfiguration {
        @Bean
        @ConditionalOnProperty(value = "spring.cloud.gateway.discovery.locator.enabled", havingValue = "false", matchIfMissing = true)
        public RedisProcessor gatewayStrategyRouteRedisProcessor() {
            return new GatewayStrategyRouteRedisProcessor();
        }
    }

    @ConditionalOnClass(ZookeeperProcessor.class)
    @ConditionalOnProperty(value = GatewayStrategyConstant.SPRING_APPLICATION_STRATEGY_GATEWAY_DYNAMIC_ROUTE_ENABLED, matchIfMissing = false)
    protected static class GatewayRouteZookeeperConfiguration {
        @Bean
        @ConditionalOnProperty(value = "spring.cloud.gateway.discovery.locator.enabled", havingValue = "false", matchIfMissing = true)
        public ZookeeperProcessor gatewayStrategyRouteZookeeperProcessor() {
            return new GatewayStrategyRouteZookeeperProcessor();
        }
    }

    @ConditionalOnClass(ConsulProcessor.class)
    @ConditionalOnProperty(value = GatewayStrategyConstant.SPRING_APPLICATION_STRATEGY_GATEWAY_DYNAMIC_ROUTE_ENABLED, matchIfMissing = false)
    protected static class GatewayRouteConsulConfiguration {
        @Bean
        @ConditionalOnProperty(value = "spring.cloud.gateway.discovery.locator.enabled", havingValue = "false", matchIfMissing = true)
        public ConsulProcessor gatewayStrategyRouteConsulProcessor() {
            return new GatewayStrategyRouteConsulProcessor();
        }
    }

    @ConditionalOnClass(EtcdProcessor.class)
    @ConditionalOnProperty(value = GatewayStrategyConstant.SPRING_APPLICATION_STRATEGY_GATEWAY_DYNAMIC_ROUTE_ENABLED, matchIfMissing = false)
    protected static class GatewayRouteEtcdConfiguration {
        @Bean
        @ConditionalOnProperty(value = "spring.cloud.gateway.discovery.locator.enabled", havingValue = "false", matchIfMissing = true)
        public EtcdProcessor gatewayStrategyRouteEtcdProcessor() {
            return new GatewayStrategyRouteEtcdProcessor();
        }
    }

    @ConditionalOnClass(TracingContext.class)
    protected static class SkywalkingStrategyConfiguration {
        @Autowired
        private ConfigurableEnvironment environment;

        //开启和关闭从SkyWalking apm-agent-core里反射获取TraceId并复制。由于SkyWalking对WebFlux上下文Threadlocal处理机制不恰当，导致产生的TraceId在全链路中并不一致，打开这个开关可以保证全链路TraceId都是一致的。缺失则默认为true
        @Bean
        @ConditionalOnProperty(value = StrategyConstant.SPRING_APPLICATION_STRATEGY_MONITOR_ENABLED, matchIfMissing = false)
        public SkyWalkingGatewayStrategyFilter skyWalkingGatewayStrategyFilter() {
            Boolean skywalkingTraceIdEnabled = environment.getProperty(GatewayStrategyConstant.SPRING_APPLICATION_STRATEGY_GATEWAY_SKYWALKING_TRACEID_ENABLED, Boolean.class, Boolean.TRUE);
            if (skywalkingTraceIdEnabled) {
                return new SkyWalkingGatewayStrategyFilter();
            }

            return null;
        }
    }
}