package com.nepxion.discovery.plugin.strategy.gateway.configuration;

/**
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.nepxion.discovery.plugin.strategy.gateway.context.GatewayStrategyContextHolder;

@Configuration
@AutoConfigureBefore(RibbonClientConfiguration.class)//代表GatewayStrategyContextAutoConfiguration会在RibbonClientConfiguration之前加载
public class GatewayStrategyContextAutoConfiguration {
    //里面有对外置参数通过 Header、Parameter以及HttpCookie 的支持。如果从外置参数里面获取不到还可以从 StrategyWrapper 里面配置的全局参数进行获取。
    @Bean
    @ConditionalOnMissingBean
    public GatewayStrategyContextHolder gatewayStrategyContextHolder() {
        return new GatewayStrategyContextHolder();
    }
}