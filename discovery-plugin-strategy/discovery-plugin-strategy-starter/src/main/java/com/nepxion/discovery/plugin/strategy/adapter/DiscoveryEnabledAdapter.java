package com.nepxion.discovery.plugin.strategy.adapter;

/**
 * 服务发现能力类，在灰度发布的时候会调用DiscoveryEnabledAdapter#apply方法进行服务实例灰度发布的选择。
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import com.nepxion.discovery.plugin.framework.loadbalance.DiscoveryEnabledLoadBalance;

public interface DiscoveryEnabledAdapter extends DiscoveryEnabledLoadBalance {

}