package com.nepxion.discovery.plugin.strategy.configuration;

/**
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.cloud.netflix.ribbon.PropertiesFactory;
import org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration;
import org.springframework.cloud.netflix.ribbon.RibbonClientName;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

import com.nepxion.discovery.plugin.framework.adapter.PluginAdapter;
import com.nepxion.discovery.plugin.strategy.adapter.DiscoveryEnabledAdapter;
import com.nepxion.discovery.plugin.strategy.constant.StrategyConstant;
import com.nepxion.discovery.plugin.strategy.rule.DiscoveryEnabledBasePredicate;
import com.nepxion.discovery.plugin.strategy.rule.DiscoveryEnabledBaseRule;
import com.nepxion.discovery.plugin.strategy.rule.DiscoveryEnabledZoneAvoidancePredicate;
import com.nepxion.discovery.plugin.strategy.rule.DiscoveryEnabledZoneAvoidanceRule;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.IRule;

@Configuration
@AutoConfigureBefore(RibbonClientConfiguration.class)
public class StrategyLoadBalanceConfiguration {
    @Autowired
    private ConfigurableEnvironment environment;

    @RibbonClientName
    private String serviceId = "client";

    @Autowired
    private PropertiesFactory propertiesFactory;

    @Autowired
    private PluginAdapter pluginAdapter;

    @Autowired(required = false)
    private DiscoveryEnabledAdapter discoveryEnabledAdapter;
    //这里构建IRule的bean实例对象,IRule主要是用来实现负载均衡的接口，包括根据serviceId(此serviceId是服务提供者的id)查询可用的服务实例，然后根据某种规则去获取其中一个实例
    @Bean
    public IRule ribbonRule(IClientConfig config) {
        if (this.propertiesFactory.isSet(IRule.class, serviceId)) {
            return this.propertiesFactory.get(IRule.class, config, serviceId);
        }

        boolean zoneAvoidanceRuleEnabled = environment.getProperty(StrategyConstant.SPRING_APPLICATION_STRATEGY_ZONE_AVOIDANCE_RULE_ENABLED, Boolean.class, Boolean.TRUE);
        if (zoneAvoidanceRuleEnabled) {
            //DiscoveryEnabledZoneAvoidanceRule内混合使用了两套断言：DiscoveryEnabledZoneAvoidancePredicate和AvailabilityPredicate
            //DiscoveryEnabledZoneAvoidancePredicate断言是继承ZoneAvoidancePredicate
            //DiscoveryEnabledZoneAvoidancePredicate断言内部组合了DiscoveryEnabledAdapter，用来实现discover的服务的灰度蓝绿功能
            //DiscoveryEnabledZoneAvoidanceRule继承了ZoneAvoidanceRuleDecorator，实现了灰度条件判断的功能，但是不包含自定义负载策略的定制
            //根据配置的加载顺序，最终生效的是DiscoveryEnabledZoneAvoidanceRule这个bean对象
            DiscoveryEnabledZoneAvoidanceRule discoveryEnabledRule = new DiscoveryEnabledZoneAvoidanceRule();
            discoveryEnabledRule.initWithNiwsConfig(config);

            DiscoveryEnabledZoneAvoidancePredicate discoveryEnabledPredicate = discoveryEnabledRule.getDiscoveryEnabledPredicate();
            discoveryEnabledPredicate.setPluginAdapter(pluginAdapter);
            discoveryEnabledPredicate.setDiscoveryEnabledAdapter(discoveryEnabledAdapter);

            return discoveryEnabledRule;
        } else {
            //DiscoveryEnabledBaseRule内混合使用了两套断言：DiscoveryEnabledBasePredicate和AvailabilityPredicate
            //DiscoveryEnabledBasePredicate断言内部组合了DiscoveryEnabledAdapter，用来实现discover的服务的灰度蓝绿功能
            //DiscoveryEnabledZoneAvoidanceRule继承了PredicateBasedRuleDecorator，该类是个抽象类，具体功能代码和ZoneAvoidanceRuleDecorator一致，只是缺少实现getPredicate()方法，实现了灰度条件判断的功能，但是不包含自定义负载策略的定制
            DiscoveryEnabledBaseRule discoveryEnabledRule = new DiscoveryEnabledBaseRule();

            DiscoveryEnabledBasePredicate discoveryEnabledPredicate = discoveryEnabledRule.getDiscoveryEnabledPredicate();
            discoveryEnabledPredicate.setPluginAdapter(pluginAdapter);
            discoveryEnabledPredicate.setDiscoveryEnabledAdapter(discoveryEnabledAdapter);

            return discoveryEnabledRule;
        }
    }
}