package com.nepxion.discovery.plugin.strategy.monitor;

/**
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.nepxion.discovery.plugin.strategy.adapter.StrategyTracerAdapter;

public class StrategyMonitorContext {
    @Autowired(required = false)
    protected StrategyTracer strategyTracer;//这个是会含有框架自带的全链路追踪组件，包含skywalking、opentracing等，需要用户自行引入依赖

    @Autowired(required = false)
    protected StrategyTracerAdapter strategyTracerAdapter;//由用户自行注入，框架没有默认的

    //这里就可以看到，对于getTraceId和getSpanId数据，如果在OpenTracing等调用链中间件引入的情况下，由调用链中间件决定，strategyTracerAdapter在这里定义不会起作用；在OpenTracing等调用链中间件未引入的情况下，strategyTracerAdapter在这里定义才有效
    public String getTraceId() {
        if (strategyTracer != null) {
            return strategyTracer.getTraceId();
        }

        if (strategyTracerAdapter != null) {
            return strategyTracerAdapter.getTraceId();
        }

        return null;
    }


    public String getSpanId() {
        if (strategyTracer != null) {
            return strategyTracer.getSpanId();
        }

        if (strategyTracerAdapter != null) {
            return strategyTracerAdapter.getSpanId();
        }

        return null;
    }

    public Map<String, String> getCustomizationMap() {
        if (strategyTracerAdapter != null) {
            return strategyTracerAdapter.getCustomizationMap();
        }

        return null;
    }
}