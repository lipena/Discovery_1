package com.nepxion.discovery.plugin.strategy.rule;

/**
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import com.nepxion.discovery.plugin.framework.decorator.ZoneAvoidanceRuleDecorator;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractServerPredicate;
import com.netflix.loadbalancer.AvailabilityPredicate;
import com.netflix.loadbalancer.CompositePredicate;

public class DiscoveryEnabledZoneAvoidanceRule extends ZoneAvoidanceRuleDecorator {
    private CompositePredicate compositePredicate;
    private DiscoveryEnabledZoneAvoidancePredicate discoveryEnabledPredicate;

    public DiscoveryEnabledZoneAvoidanceRule() {
        super();
        //该断言是继承ZoneAvoidancePredicate断言的，在ZoneAvoidancePredicate断言之上实现了discovery对服务的灰度过滤功能
        //ZoneAvoidancePredicate断言是用来判断在某个zone内服务的运行性能，如果运行性能不佳，则直接剔除该zone内的所有服务
        discoveryEnabledPredicate = new DiscoveryEnabledZoneAvoidancePredicate(this, null);
        //这个断言是用来剔除不可用的服务,如果短路或者请求数超出限制则该服务会被剔除
        AvailabilityPredicate availabilityPredicate = new AvailabilityPredicate(this, null);
        compositePredicate = createCompositePredicate(discoveryEnabledPredicate, availabilityPredicate);
    }

    private CompositePredicate createCompositePredicate(DiscoveryEnabledZoneAvoidancePredicate discoveryEnabledPredicate, AvailabilityPredicate availabilityPredicate) {
        return CompositePredicate.withPredicates(discoveryEnabledPredicate, availabilityPredicate)
                // .addFallbackPredicate(availabilityPredicate)
                // .addFallbackPredicate(AbstractServerPredicate.alwaysTrue())
                .build();
    }

    @Override
    public void initWithNiwsConfig(IClientConfig clientConfig) {
        discoveryEnabledPredicate = new DiscoveryEnabledZoneAvoidancePredicate(this, clientConfig);
        //这个断言是用来剔除不可用的服务,如果短路或者请求数超出限制则该服务会被剔除
        AvailabilityPredicate availabilityPredicate = new AvailabilityPredicate(this, clientConfig);
        compositePredicate = createCompositePredicate(discoveryEnabledPredicate, availabilityPredicate);
    }

    @Override
    public AbstractServerPredicate getPredicate() {
        return compositePredicate;
    }

    public DiscoveryEnabledZoneAvoidancePredicate getDiscoveryEnabledPredicate() {
        return discoveryEnabledPredicate;
    }
}