package com.nepxion.discovery.plugin.framework.listener.discovery;

/**
 * 获取服务列表责任链处理抽象类
 * 控制服务发现
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;

import com.nepxion.discovery.plugin.framework.listener.BasicListener;

public abstract class AbstractDiscoveryListener extends BasicListener implements DiscoveryListener {
    @Autowired
    protected DiscoveryClient discoveryClient;
    //这代码写的就很对称和工整，服务发现和服务注册和服务的负载均衡都有类似的方法，用来获取spring-cloud-common包内的元接口实例
    public DiscoveryClient getDiscoveryClient() {
        return discoveryClient;
    }
}