package com.nepxion.discovery.plugin.framework.configuration;

/**
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.netflix.ribbon.PropertiesFactory;
import org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration;
import org.springframework.cloud.netflix.ribbon.RibbonClientName;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.nepxion.discovery.plugin.framework.decorator.ZoneAvoidanceRuleDecorator;
import com.nepxion.discovery.plugin.framework.listener.loadbalance.LoadBalanceListenerExecutor;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import com.netflix.loadbalancer.ServerListFilter;
import com.netflix.loadbalancer.ServerListUpdater;
import com.netflix.loadbalancer.ZoneAwareLoadBalancer;

//RibbonClientConfiguration配置包含了很多ribbon的基本bean配置，包括IRule、ILoadBalancer、IClientConfig、IPing、ServerList等
@Configuration
@AutoConfigureAfter(RibbonClientConfiguration.class)
public class PluginLoadBalanceConfiguration {
    @RibbonClientName
    private String serviceId = "client";

    @Autowired
    private PropertiesFactory propertiesFactory;

    @Autowired
    private LoadBalanceListenerExecutor loadBalanceListenerExecutor;

    //这里加了@ConditionalOnMissingBean的注解，实际生效的不是ZoneAvoidanceRuleDecorator，而是DiscoveryEnabledZoneAvoidanceRule
    @Bean
    @ConditionalOnMissingBean
    public IRule ribbonRule(IClientConfig config) {
        if (this.propertiesFactory.isSet(IRule.class, serviceId)) {
            return this.propertiesFactory.get(IRule.class, config, serviceId);
        }

        ZoneAvoidanceRuleDecorator rule = new ZoneAvoidanceRuleDecorator();
        rule.initWithNiwsConfig(config);

        return rule;
    }

    //这里定义了ILoadBalancer的bean，虽然在PluginLoadBalanceConfiguration类上面是在RibbonClientConfiguration加载，
    // 但是RibbonClientConfiguration里面的ILoadBalancer的bean上面有@ConditionalOnMissingBean注解，所以，最终生效的ILoadBalancer是在这里定义
    @Bean
    public ILoadBalancer ribbonLoadBalancer(IClientConfig config, ServerList<Server> serverList, ServerListFilter<Server> serverListFilter, IRule rule, IPing ping, ServerListUpdater serverListUpdater) {
        //这里的serviceId会在负载均衡的场景下获取，在discover-guide项目中，gateway在这里会经过负载均衡的时候，serviceId会分别变为discovery-guide-service-a和discovery-guide-service-b，
        if (this.propertiesFactory.isSet(ILoadBalancer.class, serviceId)) {
            return this.propertiesFactory.get(ILoadBalancer.class, config, serviceId);
        }

        ZoneAwareLoadBalancer<?> loadBalancer = new ZoneAwareLoadBalancer<>(config, rule, ping, serverList, serverListFilter, serverListUpdater);
        loadBalanceListenerExecutor.setLoadBalancer(loadBalancer);

        return loadBalancer;
    }
}