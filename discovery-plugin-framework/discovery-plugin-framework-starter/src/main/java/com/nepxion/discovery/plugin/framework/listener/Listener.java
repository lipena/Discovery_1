package com.nepxion.discovery.plugin.framework.listener;

/**
 * 标记接口，继承Ordered，提供有序性
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.springframework.core.Ordered;

public interface Listener extends Ordered {

}