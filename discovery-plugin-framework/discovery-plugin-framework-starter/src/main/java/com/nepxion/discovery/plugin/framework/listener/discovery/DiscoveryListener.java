package com.nepxion.discovery.plugin.framework.listener.discovery;

/**
 * 获取服务列表责任链处理接口，注意这里的入参都是provider者的信息，比如serviceId是服务提供者的serviceId
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import java.util.List;

import org.springframework.cloud.client.ServiceInstance;

import com.nepxion.discovery.plugin.framework.listener.Listener;

public interface DiscoveryListener extends Listener {
    void onGetInstances(String serviceId, List<ServiceInstance> instances);

    void onGetServices(List<String> services);
}