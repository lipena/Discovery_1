package com.nepxion.discovery.plugin.framework.listener;

/**
 * listener基础类，包含获取基本的组件工具(PluginContextAware,PluginAdapter,PluginEventWapper)
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import org.springframework.beans.factory.annotation.Autowired;

import com.nepxion.discovery.plugin.framework.adapter.PluginAdapter;
import com.nepxion.discovery.plugin.framework.context.PluginContextAware;
import com.nepxion.discovery.plugin.framework.event.PluginEventWapper;

public class BasicListener implements Listener {
    @Autowired
    protected PluginContextAware pluginContextAware;//获取spring上下文ApplicationContext的类，包含两套方法(静态和非静态)

    @Autowired
    protected PluginAdapter pluginAdapter;//获取注册中心内的服务配置信息和元数据信息(内部含义Registration接口实现)，获取路由规则信息(缓存中取)

    @Autowired
    protected PluginEventWapper pluginEventWapper;//eventbus事件发布与订阅类

    public PluginContextAware getPluginContextAware() {
        return pluginContextAware;
    }

    public PluginAdapter getPluginAdapter() {
        return pluginAdapter;
    }

    public PluginEventWapper getPluginEventWapper() {
        return pluginEventWapper;
    }

    @Override
    public int getOrder() {
        return 0;
    }
}