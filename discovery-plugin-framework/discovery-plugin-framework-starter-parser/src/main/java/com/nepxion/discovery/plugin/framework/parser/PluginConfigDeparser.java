package com.nepxion.discovery.plugin.framework.parser;

/**
 * 配置中心的反解析器
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import com.nepxion.discovery.common.entity.RuleEntity;

public interface PluginConfigDeparser {
    String deparse(RuleEntity ruleEntity);
}