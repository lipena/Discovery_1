package com.nepxion.discovery.plugin.framework.parser;

/**
 * 配置中心的解析器，将string转换为RuleEntity
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import com.nepxion.discovery.common.entity.RuleEntity;

public interface PluginConfigParser {
    RuleEntity parse(String config);
}