package com.nepxion.discovery.common.entity;

/**
 * 对应<headers>标签体
 * 策略中配置条件表达式中的Header来决策蓝绿和灰度，可以代替外部传入Header
 * <headers>
 *      <header key="a" value="1"/>
 *  </headers>
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.util.LinkedCaseInsensitiveMap;

public class StrategyHeaderEntity implements Serializable {
    private static final long serialVersionUID = 7784567539151885177L;

    private Map<String, String> headerMap = new LinkedCaseInsensitiveMap<String>();

    public Map<String, String> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(Map<String, String> headerMap) {
        this.headerMap.clear();

        if (MapUtils.isNotEmpty(headerMap)) {
            this.headerMap.putAll(headerMap);
        }
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object object) {
        return EqualsBuilder.reflectionEquals(this, object);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}