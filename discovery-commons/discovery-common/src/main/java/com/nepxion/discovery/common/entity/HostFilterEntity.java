package com.nepxion.discovery.common.entity;

/**
 * 对应<blacklist>标签体和<whitelist>标签体
 * <blacklist filter-value="10.10;11.11">
 *             <!-- 表示下面服务，不允许172.16和10.10和11.11为前缀的IP地址注册 -->
 *             <service service-name="discovery-springcloud-example-a" filter-value="172.16"/>
 * </blacklist>
 *<whitelist filter-value="">
 *             <service service-name="" filter-value=""/>
 * </whitelist>
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class HostFilterEntity implements Serializable {
    private static final long serialVersionUID = 3830016495318834467L;

    private FilterType filterType;//只支持白名单和黑名单两种
    private List<String> filterValueList;//host地址全局过滤列表
    private Map<String, List<String>> filterMap = new LinkedHashMap<String, List<String>>();//host地址过滤表,key为serviceId,value为serviceId对应的host地址

    public FilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }

    public List<String> getFilterValueList() {
        return filterValueList;
    }

    public void setFilterValueList(List<String> filterValueList) {
        this.filterValueList = filterValueList;
    }

    public Map<String, List<String>> getFilterMap() {
        return filterMap;
    }

    public void setFilterMap(Map<String, List<String>> filterMap) {
        this.filterMap = filterMap;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object object) {
        return EqualsBuilder.reflectionEquals(this, object);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}