package com.nepxion.discovery.common.entity;

/**
 * 对应<count>标签体
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class CountFilterEntity implements Serializable {
    private static final long serialVersionUID = 3830016495318834467L;

    private Integer filterValue;//全局处理的count值，这个值是serviceId对应的服务实例数的最大值  filter-value="10.10;11.11"
    //底下这个是 <!-- 表示下面服务，不允许172.16和10.10和11.11为前缀的IP地址被发现 -->
    //            <service service-name="discovery-springcloud-example-b" filter-value="172.16"/>
    private Map<String, Integer> filterMap = new LinkedHashMap<String, Integer>();//局部处理的count值,key是serviceId

    public Integer getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(Integer filterValue) {
        this.filterValue = filterValue;
    }

    public Map<String, Integer> getFilterMap() {
        return filterMap;
    }

    public void setFilterMap(Map<String, Integer> filterMap) {
        this.filterMap = filterMap;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object object) {
        return EqualsBuilder.reflectionEquals(this, object);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}