package com.nepxion.discovery.common.entity;

/**
 *<version>标签体
 * <!-- 表示网关g的1.0，允许访问提供端服务a的1.0版本 -->
 *             <service consumer-service-name="discovery-springcloud-example-gateway" provider-service-name="discovery-springcloud-example-a" consumer-version-value="1.0" provider-version-value="1.0"/>
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class VersionFilterEntity implements Serializable {
    private static final long serialVersionUID = -6147106004826964165L;
    //key是consumer-service-name
    private Map<String, List<VersionEntity>> versionEntityMap = new LinkedHashMap<String, List<VersionEntity>>();

    public Map<String, List<VersionEntity>> getVersionEntityMap() {
        return versionEntityMap;
    }

    public void setVersionEntityMap(Map<String, List<VersionEntity>> versionEntityMap) {
        this.versionEntityMap = versionEntityMap;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object object) {
        return EqualsBuilder.reflectionEquals(this, object);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}